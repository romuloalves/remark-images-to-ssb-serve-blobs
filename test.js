const test = require('tape');
const unified = require('unified');
const remarkParse = require('remark-parse');
const inspect = require('unist-util-inspect');
const imagesToSsbServeBlobs = require('./index');

test('it lifts a nested list to the root level', t => {
  t.plan(2);

  const markdown = `
# Title

Take a look at this scenery:

![scenery](&Pe5kTo/V/w4MToasp1IuyMrMcCkQwDOdyzbyD5fy4ac=.sha256)
`;

  const actualInput = unified()
    .use(remarkParse, {commonmark: true})
    .parse(markdown);

  // console.log(JSON.stringify(actualInput));
  console.log(inspect(actualInput));

  const expectedInput = {
    type: 'root',
    children: [
      {
        type: 'heading',
        depth: 1,
        children: [
          {
            type: 'text',
            value: 'Title',
            position: {
              start: {line: 2, column: 3, offset: 3},
              end: {line: 2, column: 8, offset: 8},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 2, column: 1, offset: 1},
          end: {line: 2, column: 8, offset: 8},
          indent: [],
        },
      },
      {
        type: 'paragraph',
        children: [
          {
            type: 'text',
            value: 'Take a look at this scenery:',
            position: {
              start: {line: 4, column: 1, offset: 10},
              end: {line: 4, column: 29, offset: 38},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 4, column: 1, offset: 10},
          end: {line: 4, column: 29, offset: 38},
          indent: [],
        },
      },
      {
        type: 'paragraph',
        children: [
          {
            type: 'image',
            title: null,
            url: '&Pe5kTo/V/w4MToasp1IuyMrMcCkQwDOdyzbyD5fy4ac=.sha256',
            alt: 'scenery',
            position: {
              start: {line: 6, column: 1, offset: 40},
              end: {line: 6, column: 65, offset: 104},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 6, column: 1, offset: 40},
          end: {line: 6, column: 65, offset: 104},
          indent: [],
        },
      },
    ],
    position: {
      start: {line: 1, column: 1, offset: 0},
      end: {line: 7, column: 1, offset: 105},
    },
  };
  t.deepEquals(actualInput, expectedInput, 'input looks good');

  const actualOutput = imagesToSsbServeBlobs()(actualInput);

  // console.log(JSON.stringify(actualOutput));
  console.log(inspect(actualOutput));

  const expectedOutput = {
    type: 'root',
    children: [
      {
        type: 'heading',
        depth: 1,
        children: [
          {
            type: 'text',
            value: 'Title',
            position: {
              start: {line: 2, column: 3, offset: 3},
              end: {line: 2, column: 8, offset: 8},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 2, column: 1, offset: 1},
          end: {line: 2, column: 8, offset: 8},
          indent: [],
        },
      },
      {
        type: 'paragraph',
        children: [
          {
            type: 'text',
            value: 'Take a look at this scenery:',
            position: {
              start: {line: 4, column: 1, offset: 10},
              end: {line: 4, column: 29, offset: 38},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 4, column: 1, offset: 10},
          end: {line: 4, column: 29, offset: 38},
          indent: [],
        },
      },
      {
        type: 'paragraph',
        children: [
          {
            type: 'image',
            title: null,
            url:
              'http://localhost:26835/%26Pe5kTo%2FV%2Fw4MToasp1IuyMrMcCkQwDOdyzbyD5fy4ac%3D.sha256',
            alt: 'scenery',
            position: {
              start: {line: 6, column: 1, offset: 40},
              end: {line: 6, column: 65, offset: 104},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 6, column: 1, offset: 40},
          end: {line: 6, column: 65, offset: 104},
          indent: [],
        },
      },
    ],
    position: {
      start: {line: 1, column: 1, offset: 0},
      end: {line: 7, column: 1, offset: 105},
    },
  };

  t.deepEquals(actualOutput, expectedOutput, 'output looks good');
});
