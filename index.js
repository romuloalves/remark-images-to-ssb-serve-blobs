const ref = require('ssb-ref');
const visit = require('unist-util-visit');
const toUrl = require('ssb-serve-blobs/id-to-url');

function imagesToSsbServeBlobs() {
  return ast => {
    visit(ast, 'image', image => {
      if (ref.isBlob(image.url)) {
        image.url = toUrl(image.url);
      }
      return image;
    });
    return ast;
  };
}

module.exports = imagesToSsbServeBlobs;
